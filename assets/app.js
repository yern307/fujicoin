/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

require('bootstrap');
import Swal from 'sweetalert2';
import './styles/app.scss';

const $dropdownToggle = $(".dropdown-toggle");
const $dropdownMenu = $(".dropdown-menu");
const showClass = "show";

$(document).ready(function() {

    $('button').click( function() {
        $('.collapse').collapse('hide');
    });

    $('.reveal').css('opacity', '0');

    $(window).scroll(function () {
        let reveals = $('.reveal');

        for (let i = 0; i < reveals.length; i++) {
            let windowHeight = window.innerHeight;
            let revealTop = reveals[i].getBoundingClientRect().top;

            if (revealTop < windowHeight - 150) {
                $(reveals[i]).css('opacity', 1);
                $(reveals[i]).css('transition', '5s');
            }
        }
    });

    $("img.img-zoom").click(
        function() {
            $(this).css('margin', '0');
            Swal.fire({
                imageUrl: $(this).attr('src'),
                imageAlt: 'A tall image',
                imageHeight: '100%',
                imageWidth:'100%',
                width:'70%',
                padding: '0em',
                customClass: {
                    image: 'm-0',
                },
                showConfirmButton: false,
            });
        });

    $(".card").click(function() {
        $(this).toggleClass("flipped");
    })

    $(".dropdown").hover(
            function() {
                const $this = $(this);
                $this.addClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "true");
                $this.find($dropdownMenu).addClass(showClass);
            },
            function() {
                const $this = $(this);
                $this.removeClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "false");
                $this.find($dropdownMenu).removeClass(showClass);
            }
        );
});