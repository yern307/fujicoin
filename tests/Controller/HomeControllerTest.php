<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class HomeControllerTest extends WebTestCase
{
    private $client = null;

    public function SetUp(): void
    {
        $this->client = static::createClient();
    }

    public function testPageReturn200(): void
    {
        $array = [
            '/', '/en/', '/en/what_is_fujicoin', '/en/team', '/en/fujicoin_foundation',
            '/en/wallet', '/en/buy_fujicoin', '/en/mining', '/en/where_to_spend',
            '/en/download', '/en/press', '/en/promotional_graphics',
            '/en/blockchain_explorers', '/en/terms_of_use',
        ];

        foreach($array as $item) {
            $this->client->request('GET', $item);
            $this->assertResponseIsSuccessful();
        }
    }
}