meta:
  description: Willkommen bei der nächsten Generation von Kryptowährung. FujiCoin ist eine international registrierte Kryptowährung. Sie ist schnell, sicher und einfach zu bedienen.

header:
  about: Über
  resources: Ressourcen
  get_started: Legen Sie jetzt los
  what_is_fujicoin: Was ist FujiCoin?
  team: Team
  community: Community
  fujicoin_fundation: FujiCoin Foundation
  mailing_list: Mailing Liste
  freenode: Freenode
  press: Presse
  promotional_graphics: Werbegrafiken
  blockchain_explorer: Blockchain Explorer
  api: Api
  node: Knoten
  wallet: Wallet
  download: Download
  buy_fujicoin: FujiCoin kaufen
  mining: Mining
  where_to_spend: Wo man FujiCoin ausgeben kann

footer:
  terms_of_use: Nutzungsbedingungen
  contact: Kontakt
  copyright: Copyright ©
  detail_copyright: Alle Rechte vorbehalten.

node:
  title: Knoten
  text1: FujiCoin Knotenliste
  subversion: Unterversion
  address: Addresse
  port: Port
  status: Status
  open: OFFEN
  closed: GESCHLOSSEN

team:
  title: FujiCoin Team
  member: Mitglieder
  web_developer: Web Entwickler

where_to_spend:
  title: Wo man FujiCoin ausgeben kann
  bright_futur:
    title: Die strahlende Zukunft
    text1: Wir haben vor, einen FujiCoin-Marktplatz zu schaffen, auf dem viele Produkte mit FujiCoin erworben werden können.

press:
  title: Presse
  press_inquiries:
    title: Für Presseanfragen
    text1: Bitte kontaktieren Sie FujiCoin.org unter der E-Mail-Adresse
  promotional:
    title: Logos und Werbegrafiken
    find_them1: Finden Sie
    here: hier
    find_them2: herunter.
  intro:
    title: Eine kurze Einführung zu FujiCoin
    find_it1: Finden Sie
    here: hier
    find_it2: herunter.

api:
  title: Api
  text1: FujiCoind-API
  command_uri: Befehl
  current_value: Aktueller Wert GMT
  height: Höhe
  block_reward: Blockbelohnung
  total_amount: Zirkulierende Anzahl
  difficulty: Schwierigkeit
  hash_per_seconds: Hash pro Sekunde

terms_of_use:
  title: Nutzungsbedingungen
  text1: FujiCoin ist nur für legale Zwecke bestimmt und sollte nicht auf andere Weise verwendet werden. Weder FujiCoin, noch einer seiner Entwickler oder Mitarbeiter unterstützen illegale Aktivitäten und werden diese in keiner Weise unterstützen.
  text2: Soweit uns bekannt ist, ist die Verwendung von FujiCoin in den meisten Rechtsordnungen nicht illegal. Einige Gerichtsbarkeiten können jedoch Beschränkungen für die Verwendung von Kryptowährungen und/oder Tools zum Schutz der Privatsphäre auferlegen, so dass Benutzer in diesen Gerichtsbarkeiten dies vor der Verwendung von FujiCoin berücksichtigen sollten. Wenden Sie sich im Zweifelsfall an einen Anwalt vor Ort, um zu vermeiden, dass Sie etwas tun, was in Ihrer Gerichtsbarkeit als illegal angesehen werden könnte.
  text3: Der Inhalt dieser Website oder anderer mit FujiCoin verbundener Websites stellt keine Rechtsberatung dar. Wenn Sie rechtliche Zweifel haben oder Hilfe benötigen, wenden Sie sich bitte an einen Anwalt in Ihrem Rechtsgebiet.
  text4: Der Inhalt dieser Website oder anderer mit FujiCoin verbundener Websites stellt keine Anlageberatung dar. Sie sollten Ihr eigenes Urteilsvermögen nutzen und mehrere Informationsquellen recherchieren, bevor Sie in FujiCoin investieren.

fujicoin_fundation:
  title: FujiCoin Foundation
  bright_futur:
    title: Strahlende Zukunft
    text1: Wir möchten in Zukunft mit der Gründung der FujiCoin Foundation sozial-philanthropische Aktivitäten unterstützen, sobald es wirtschaftlich machbar ist.

promotional_graphics:
  title: Werbegrafiken
  graphics_kit: Grafik-Kit
  get_graphics1: 'Laden Sie sich die Grafiken '
  here: hier
  get_graphics2: herunter.

blockchain_explorer:
  title: Blockchain Explorer
  blockbook:
    title: FujiCoins Offizieller Block Explorer
    text1: Sie können nach Block, Transaktionen oder FujiCoin-Adressen suchen.
  disclaimer:
    title: HAFTUNGSAUSSCHLUSS
    text1: Dieser Link wird nur zu Informationszwecken bereitgestellt. Der hier aufgeführte Service wurde nicht von FujiCoin.org bewertet oder unterstützt und es wird keine Garantie für die Richtigkeit dieser Informationen gegeben. Bitte lassen Sie bei der Nutzung von Diensten Dritter Vorsicht walten.

what_is_fujicoin:
  title: Was ist FujiCoin?
  description:
    card_title: Übersicht
    title: FujiCoin wurde am Samstag, 28. Juni 2014 21:00:00 GMT gestartet
    text1: FujiCoin ist eine Kryptowährung, die sofortige, nahezu kostenfreie Zahlungen an jeden in der Welt ermöglicht.
    text2: FujiCoin ist ein offenes globales Zahlungsnetzwerk, das vollständig dezentralisiert ist. Mathematik sichert das Netzwerk ab und ermöglicht es dem Einzelnen, seine eigenen Finanzen zu kontrollieren.
    text3: Die Zeit, die für die Transaktionsbestätigung von FujiCoin benötigt wird, beträgt ein Zehntel des Bitcoins, sie wurde stark beschleunigt.
    text4: Da die Verarbeitungskapazität der Transaktion das 10-fache von Bitcoin beträgt, muss man sich auch keine Sorgen über die Verzögerung der Überweisung machen.
    text5: Mit erheblicher Unterstützung der Industrie, dem Handelsvolumen und der Liquidität ist FujiCoin ein bewährtes Handelsmedium als Ergänzung zu Bitcoin und Litecoin.
  s_curve:
    title: S-Kurve
    text1: Es wird angenommen, dass die Verbreitung von Innovationen im Allgemeinen der S-Kurve folgt. Deshalb haben wir FujiCoin so konzipiert, dass auch die Verbreitung der Kryptowährung der S-Kurve folgt. Um seinen Wert zu stabilisieren, sollte er entsprechend dem Grad der Verbreitung (Nachfrage) zugeführt werden.
    text2: Eine Programmierung zur Anpassung der Zuführleistung an den Bedarf der Zukunft ist mit der heutigen Technik nicht machbar. Stattdessen haben wir angenommen, dass die Nachfrage langfristig gemäß der S-Kurven-Theorie der Verteilung verläuft.
    text3: Wir glauben, dass es notwendig ist, dem FujiCoin ein paar Jahrzehnte zu widmen, damit er sich weit und breit in der Welt etablieren kann. Nachfolgend ist die Verteilungskurve abgebildet, die wir mit Hilfe der S-Kurve realisiert haben. Der FujiCoin wird entsprechend dieser Kurve ausgegeben.
    text4: FujiCoin ist keine Kryptowährung, die für spekulative Zwecke gemacht ist. Vielmehr ist es unser Ziel, Menschen auf der ganzen Welt zu ermöglichen, FujiCoin täglich zu nutzen.
  specifications:
    title: Spezifikationen
    algorithm:
      title: Algorithmus
      text1: Scrypt-N11
    block_interval:
      title: Block Intervall
      text1: 1 Minute (60 Sekunden)
    difficulty_retarget:
      title: Anpassung der Schwierigkeit
      text1: bei jedem Block (unter Verwendung von Kimotos Gravity Well)
    total_coins_amount:
      title: Gesamtzahl der Coins
      text1: 10,000,000,000 (10 Milliarden)
    premine:
      title: Pre-Mine
      text1: 1,6 % dafür, dass Sie Menschen auf der ganzen Welt ermutigen, FujiCoin zu nutzen.
    block_reward_curve:
      title: Block-Belohnungskurve
      text1: S-Kurven-Theorie (Gonpertz-Kurve)
    currency_unit:
      title: Währungskürzel
      text1: FJC

home:
  title: Japanische Digitale Währung
  internationally_registerd:
    title1: FujiCoin 3.0
    title2: FujiCoin ist eine international registrierte Kryptowährung.
    text1: FujiCoin ist eine Kryptowährung, die sofortige, nahezu kostenfreie Zahlungen an jeden in der Welt ermöglicht. FujiCoin ist ein offenes globales Zahlungsnetzwerk, das vollständig dezentralisiert ist.
    text2: Weitere Informationen

  anniversary:
    title: . Jahrestag der Gründung
    text1: Seit dem Start im Juni 2014 schlägt das Herz von FujiCoin ununterbrochen weiter und verbindet die Blockchain mit Sicherheit.
  the_best:
    title: Die beste Kryptowährung
    text1: Welcher Coin führt das Erbe des Japaners Satoshi Nakamoto weiter?
    text2: Wenn Sie sich die Vergleichstabelle ansehen, dürfte die Antwort leicht fallen. Es ist FujiCoin!
  new_generation:
    title: Nächste Generation
    text1: Willkommen bei der nächsten Generation von Kryptowährung. FujiCoin ist schneller, sicherer und einfacher zu bedienen.
  quick:
    title: Schnelligkeit
    text1: Unsere Bestätigungszeit ist zehnmal kürzer als das Bitcoin-Netzwerk, was es zuverlässig für zeitkritische Transaktionen macht.
  low_fees:
    title: Niedrige Gebühren
    text1: Niedrige Transaktionsgebühren. Das Versenden von FujiCoin ist so günstig, dass Mikrotransaktionen wirtschaftlich machbar sind.
  peer_to_peer:
    title: Peer-To-Peer
    text1: FujiCoin verwendet ein dezentrales Blockchain-Netzwerk, so dass es keine zentrale Instanz gibt, der man vertrauen muss.
  large_capacity:
    title: Hohe Kapazität
    text1: Die Transaktionskapazität ist zehnmal größer als bei Bitcoin. Sie müssen sich keine Sorgen über Transaktionsstaus machen.
  secure:
    title: Sicherheit
    text1: Erweiterte Verschlüsselung und umfassende Sicherheit.
  global:
    title: Global
    text1: Sie können Geld an jeden Ort der Welt senden.
  market:
    title: Markt
    text1: So kaufen und verkaufen Sie FujiCoin.
    crex24: Crex24
    ataix: Ataix
    barterdex: Baterdex
    unnamed: Unnamed
    northern: Northern
    blockbid:  BlockBid

wallet:
  title: Wallet
  light:
    title: Light Wallet
    electrum:
      title: Electrum-FJC
      text1: Offizielle FujiCoin-Wallet.
    mobile_electrum:
      title: Mobile Electrum-FJC
      text1: Mobile Version der offiziellen FujiCoin-Wallet.
      text2: Sie ist klein, Anwenderfreundlich sowie Sicher.
      text3: FujiCoin.org empfiehlt das Electrum-FJC-Wallet zu verwenden.
    monya:
      title: Monya
      text1: Open-Source-Wallet für mehrere Währungen
      text2: Unterstützt BIP-32 + BIP44.
      text3: Sie ist klein, Anwenderfreundlich sowie Sicher.
  dex:
    title: DEX-Wallet
    atomic:
      title: AtomicDEX
      text1: AtomicDEX ist eine 100% nicht haftpflichtige Multi-Coin Wallet und Atomic Swap DEX. AtomicDEX-Benutzer speichern Coins in ihren eigenen Wallets und machen Cross-Chain-Swaps, während sie immer die Kontrolle über ihre privaten Schlüssel haben.
  hardware:
    title: Hardware-Wallet
    trezor:
      title: TREZOR
      text1: TREZOR ist der Marktführer in sachen Hardware-Wallet.
      text2: Durch das Koppeln von TREZOR an das Electrum-FJC-Wallet erhalten Sie höchste Sicherheit.
      text3: Sie können FujiCoin auch mit TREZOR Wallet (Web-Wallet) verwenden.
      text4: Den Link dazu gibts hier
    archos:
      title: ARCHOS Safe-T mini
      text1: Safe-T mini zeichnet sich durch sehr elegantes Design aus.
      text2: Durch den Anschluss des Safe-T mini an das Electrum-FJC-Wallet erhalten Sie höchste Sicherheit.
  core:
    title: Core Wallet
    text1: Das FujiCoin Core Wallet basiert auf dem bekannten Bitcoin Core QT Wallet. Wer sich damit auskennt, wird in kürzester Zeit mit der FujiCoin-Wallet zurechtkommen.
    text2: So installieren Sie das Core Wallet
    install_core:
      title: Windows
      text1: Entpacken Sie die Datei an eine beliebige Stelle. Öffnen Sie dann den FujiCoin-Ordner und führen Sie fujicoin-qt.exe aus.
      text2: Wenn Sie das Wallet zum ersten Mal starten, wird es eine längere Zeit dauern, bis Ihre DB mit dem FujiCoin-Netzwerk synchronisiert ist.
      text3: Ihre DB befindet sich standardmäßig im Ordner 'C:\Users\%USERNAME%\AppData\Roaming\Fujicoin'.
    short_period:
      title: So installieren Sie sie in kürzester Zeit.
      text1: Wenn Sie das Wallet zum ersten Mal Installieren, wird es eine längere Zeit dauern, bis Ihre DB mit dem FujiCoin-Netzwerk synchronisiert ist. Derzeit dauert es etwa 12 Stunden, in Zukunft wird es länger dauern. Deshalb hat FujiCoin beschlossen, einen Service anzubieten, der in etwa 30 Minuten installiert werden kann. Wir stellen einmal im Monat die aktuelle Datenbank zur Verfügung. Bitte laden Sie die aktuelle Datenbank unter folgendem Link herunter und führen Sie die folgenden Schritte aus.
      text2: Entpacken Sie die heruntergeladene Datenbank nach 'C:\Users\%USERNAME%\AppData\Roaming\Fujicoin'.
      text3: Die Datenbank wurde unter Roaming\Fujicoin entpackt.
      text4: Starten Sie fujicoin-qt.exe, die Sie bereits Installiert haben.
      text5: Ganz einfach! Diese Aktualisierungsmethode kann auch verwendet werden, wenn es länger her ist, dass Sie Ihr FujiCoin-Wallet zuletzt synchronisiert haben.
      text6: Wir empfehlen die alte Datenbank zu löschen.
      text7: Entpacken Sie die heruntergeladene Datenbank ähnlich wie oben beschrieben.
      text8: Starten Sie fujicoin-qt.exe.
      text9: Mit diesem Verfahren können Sie Ihr Wallet in kürzester Zeit mit dem Netzwerk synchronisieren.
    example_configuration:
      title: 'Beispiel einer Konfigurationsdatei: ''fujicoin.conf'''
      text1: Sie können die folgenden Parameter nach Bedarf einstellen. Legen Sie die Konfigurationsdatei in 'Roaming\Fujicoin\' ab.
    wallet_address_tool:
      title: Wallet-Adressen-Tool
      text1: BIP39 Tool ist ein Tool zum Generieren eines Satzes verschiedener Adressen, öffentlicher und privater Schlüssel. Fachwissen ist notwendig, es hilft im Ernstfall bei der Wiederherstellung von Vermögenswerten etc.
      text2: 'Hinweis: Es wird nicht empfohlen, Mnemonic, Passphrase oder Secret Key auf einer externen Seite einzugeben. Sie sollten es unter einem beliebigen Namen speichern oder herunterladen und dann in Ihrer lokalen Umgebung ausführen.'
      text3: GitHub
      text4: Download
  paper_wallet:
    title: Papier Wallet
    text1a: 'Ein Papier Wallet ist für Benutzer von FujiCoin verfügbar. Es kann von der Download-Seite bezogen werden. Anweisungen zur Verwendung finden Sie unter '
    here: hier 
    text1b: .
    text2: Es ist zu beachten, dass Sie für die Einzahlung in Ihre PC-Geldbörse mit dem privaten Schlüssel die Konsole des Debug-Fensters verwenden müssen.
    text3: Bitte führen Sie folgenden Befehl in der Konsole aus.
    text4: importprivkey [private key]

buy_fujicoin:
  title: FujiCoin kaufen
  exchanges:
    title: Börsen
    crex24:
      title: CREX24
      text1: 'Gebühr: Handel <= 0,1 %'
      text2: Wenn Sie in einem Land leben, welches vom Handel ausgeschlossen ist, können Sie einen VPN, bevorzugt mit japanischer IP verwenden.
      text3: Zum Beispiel
    ataix:
      title: ATAIX
      text1: 'Gebühr: Handel 0,1-0,5 % (variiert je nach monatlichem Volumen)'
    unnamed:
      title: Unnamed
      text1: 'Gebühr: Handel 0,2 %'
    northern:
      title: Northern
      text1: 'Gebühr: Handel 0,15 %'
      text2: FujiCoin Handelspaare
    blockdx:
      title: Block DX
      text1: 'Gebühren: geringe Gebühr'
      text2: FujiCoin Handelspaare BTC, LTC und viele mehr!
      text3: BlockDX ist ein idealer DEX mit BIP65 Atomic Swap Technologie.
    barterdex:
      title: BarterDEX
      text1: 'Gebühren: niedrige Gebühr'
      text2: 'FujiCoin Handelspaare: BTC, LTC, und viele mehr!'
      text3: BarterDEX befindet sich derzeit in der BETA.
    blockbid:
      title: Blockbid
      text1: Demnächst!
  credit_card:
    title: Kreditkarte
    coin2001:
      title: Coin2001
      text1: Bei Coin2001 können Sie FujiCoin sofort mit einer Kreditkarte kaufen.
    indacoin:
      title: Indacoin
      text1: Auf der Indacoin-Website oder Wallet können Sie FujiCoin sofort mit einer Kredit- oder Debitkarte kaufen.

  market_price:
    title: Marktpreis
    coinmarketcap:
      title: CoinMarketCap
      text1: Prüfen Sie, wie der FujiCoin-Markt sich entwickelt!

mining:
  title: Mining
  how_to_mine:
    title: Übersicht
    text1: FujiCoin verwendet einen speziellen Algorithmus namens Script-N11 für den Proof of Work.
    text2: FujiCoin kann derzeit mit ASIC, GPUs und CPUs gemint werden.
    text3: -N11 bedeutet, dass der N-Faktor von Script-N auf 11 festgelegt ist. Das liegt daran, dass wir einem großen N-Faktor nicht trauen.
  basic:
    title: Informationen
    text1: Wichtige Hinweise zu den für das Mining verwendeten Adressen
    text2: Stellen Sie sicher, dass Sie die Core Wallet-Adresse für das Mining verwenden.
    text3: Verwenden Sie nicht die Adresse des Electrum-FJC-Wallets.
    text4: Electrum kann nicht mit Miner-Adressen umgehen, da der Adressverlauf durch die Electrum-Server begrenzt ist.
  gpu_mining:
    title: GPU-Mining-Software
    text1: ccminer für NVIDIA GPU
    text2: sgminer für AMD GPU
  cpu_mining:
    title: CPU Mining Software
    text1: cpuminer-multi für Linux und Windows
  pool:
    title: Pool
    text1: Um die Dezentralisierung der Pools voranzutreiben, wählen Sie bitte einen Pool mit kleiner Hashrate.
    fujicoin:
      title: FujiCoin.org
      text1: 1% Gebühr!
      text2: Wählen Sie den Pool aus, alle Informationen über das Mining werden Sie dort finden.
      pool1: pool1
      pool2: pool2
      pool3: pool3
      pool4: pool4
      text7: 'Port 3031:  Anfangsschwierigkeit = 32 [nur für CPU]'
      text8: 'Port 3032:  Anfangsschwierigkeit = 128 [für mid range GPU]'
      text9: 'Port 3033:  Anfangsschwierigkeit = 512 [für high performance GPU]'
      text10: 'Port 3034:  Anfangsschwierigkeit = 4096 [für Rigs]'
      text11: 'Port 3035: Anfangsschwierigkeit = 32768 [für Farms]'
    zpool:
      title: Zpool
      text1: 0.9%Gebühr!
  how_to_operate:
    title: So betreiben Sie den Pool
    text1: Die Verwendung von NOMP für FujiCoin macht es einfach, den Pool zu betreiben. Bitte beachten Sie das folgende GitHub-Repository.
    text2: Empfohlene Umgebung
    link: Link
    text3: 'OS: Es wird Ubuntu 20.04 oder 18.04 empfohlen.'
    text4: 'CPU: Die CPU-Last ist gering.'
    text5: 'Speicher: 4 GB empfohlen.'
    text6: 'Speicherplatz: 20 GB oder mehr sind erforderlich.'

download:
  title: Download
  electrum:
    title: Electrum-FJC
    text1: Electrum-FJC ist das leichtgewichtige Wallet für FujiCoin. Es wird für den allgemeinen Gebrauch empfohlen.
    text2: 'Letzte Aktualisierung: 2. Oktober 2020'
    text3: 'Version 3.3.9: 2019-11-16'
    text4: ・Synchronisationsprobleme behoben
    text5: RELEASE-HINWEISE
    windows:
      title: Windows
      text1: Für Windows 64bit/32bit O/S
      text2: Setup-Version, portable Version und exe-Version sind verfügbar.
    apple:
      title: Apple
      text1: Für Mac OS X
      text2: Kompatibel mit High Sierra (10.13) oder höher.
    android:
      title: Android
      text1: Für Android-Geräte
      text2: Bitte installieren Sie die .apk-Datei.
    github:
      title: GitHub
      text1: Quellcode
  core:
    title: FujiCoin Core Wallet
    text1: Core Wallet ist die Software, die Sie zum Senden, Empfangen und Speichern von FujiCoin verwenden. Für den professionellen Einsatz
    text2: 'Letzte Aktualisierung: 1. Mai 2021'
    text3: 'Version 0.21.1: 01. Mai 2021'
    text4: Taproot Soft Fork
    text5: In dieser Version sind die Mainnet- und Testnet-Aktivierungsparameter für den taproot-Softfork (BIP341) enthalten, der außerdem Unterstützung für Schnorr-Signaturen (BIP340) und tapscript (BIP342) bietet. Diese Verbesserungen ermöglichen es Nutzern von Einzelsignatur-Skripten, Multisignatur-Skripten und komplexen Verträgen, alle identisch aussehende Verpflichtungen zu verwenden, die ihre Privatsphäre und die Fungibilität aller FujiCoins verbessern. Die Aktivierung ist bereits abgeschlossen, so dass Sie diese neuen Funktionen sofort im Mainnet nutzen können.
    text6: Das Erstellen eines privaten Signet-Netzwerks ist etwas einfacher geworden.
    text7: Holen Sie sich unten das FujiCoin-signet-utility, um Ihr eigenes Signet-Netzwerk zu bauen.
    text8: Im Folgenden wird beschrieben, wie Sie FujiCoin-signet-utility verwenden.
    text9: RELEASE-HINWEISE
    windows:
      title: Windows
      text1: FujiCoin QT Wallet 64-Bit
      text2: Installationsprogramm oder Zip-Datei
    linux:
      title: Linux
      text1: für x86_64 i686-pc arm
      text2: aarch64 riscv64
      text3: statisch gelinkte ausführbare Datei
    macosx:
      title: Mac OS X
      text1: FujiCoin-qt 64bit
      text2: Installationsprogramm oder Zip-Datei
  raw_blockchain:
    title: Rohdatenbank der Blockchain
    text1: Beim ersten Start des vollen FujiCoin-Knotens dauert es sehr lange, bis er sich mit dem Netzwerk synchronisiert.
    text2: Durch die Verwendung der Raw Blockchain-Datenbank ist es möglich, sich in kurzer Zeit mit dem Netzwerk zu synchronisieren.
    text3: Diese Datenbank kann sowohl für Windows als auch für Linux verwendet werden.
    text4: 'Standard-DB: Fujicoin-DB-txi0-YYMMDD.zip'
    text5: 'Mit Tx Index: Fujicoin-DB-txi1-YYMMDD.zip'
    text6: Download Center
  artwork:
    title: Artwork-Kit
    text1: Satz von Logo und Favicon in verschiedenen Größen für Entwickler
  disclaimer:
    title: Nutzungsbedingungen
    text1: Durch die Nutzung dieser Software erkennen Sie an und verstehen, dass FujiCoin.org nicht für die Nutzung in illegalen Aktivitäten vorgesehen ist und dass keine Person oder Einrichtung, die mit der Erstellung, Entwicklung, Vermarktung oder Förderung von FujiCoin verbunden ist, für die Nutzung durch eine Person, Gruppe oder Einrichtung verantwortlich gemacht werden kann, die gegen das Gesetz in ihrer jeweiligen Gerichtsbarkeit verstößt.
    text2: Das Risiko, dass etwas schief geht, ist sehr gering. Die Software befindet sich jedoch noch in der Entwicklung. Das bedeutet, dass Dinge kaputt gehen können und es keine Garantien dafür gibt. Verwenden Sie es auf eigenes Risiko, befolgen Sie die Anweisungen genau und setzen Sie nur Geld ein, das Sie sich leisten können, zu verlieren, falls etwas schief geht.

error:
  error500:
    title: Error 500
    subtitle: Oh Oh! Serverfehler.
    text1: 'Der Server kann diesen Inhalt nicht liefern. Bitte versuchen Sie es später nocheinmal. Für den Moment können Sie auf die Schaltfläche unten klicken, um zurück zur Startseite zu gelangen.'
  error404:
    title: Error 404
    subtitle: Oh Oh! Hier geht's leider nicht weiter! Wir arbeiten dran.
    text1: 'Die gesuchte Seite existiert nicht. Wie Sie hierher gekommen sind, ist wohl Zauberei. Sie können jedoch auf die Schaltfläche unten klicken, um zur Startseite zurückzukehren.'
  error:
    title: Fehler
    subtitle: Oh Oh! Fehler.
    text1: 'Ein Fehler ist aufgetreten. Sie können auf die Schaltfläche unten klicken, um zurück zur Startseite zu gelangen.'
