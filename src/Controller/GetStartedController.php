<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class GetStartedController extends AbstractController
{
    public function wallet(): Response
    {
        $wallet = ['lightweight', 'dex', 'hardware', 'core', 'paper_wallet'];

        return $this->render('get_started/wallet/wallet.html.twig', ['wallet' => $wallet]);
    }
    public function buyFujicoin(): Response
    {
        $buy = ['exchange', 'credit_card', 'market_price'];

        return $this->render('get_started/buy_fujicoin/buy_fujicoin.html.twig', ['buy' => $buy]);
    }
    public function mining(): Response
    {
        $mining = ['how_to_mine', 'basic_info', 'gpu_miner', 'cpu_miner', 'pools', 'how_to_operate'];

        return $this->render('get_started/mining/mining.html.twig', ['mining' => $mining]);
    }
    public function whereToSpend(): Response
    {
        return $this->render('get_started/where_to_spend.html.twig');
    }
    public function download(): Response
    {
        $download = ['electrum', 'core', 'raw', 'artwork'];

        return $this->render('get_started/download/download.html.twig', ['download' => $download]);
    }
}
